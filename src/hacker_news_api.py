import logging
import requests
import argparse

class HNUrlCreator:
    def __init__(self):
        self.base_url = "https://hacker-news.firebaseio.com/v0/"
        self.pretty_print_string = "print=pretty"
        self.top_string = "topstories"

    def _create_url(self, specific_path):
        url = ''.join([self.base_url, specific_path, '.json', '?', self.pretty_print_string])
        logging.debug(url)
        return url

    def get_top_stories_url(self):
        return self._create_url(self.top_string)

    def get_item_url(self, item_id):
        specific_path = '/'.join(['item', item_id])
        return self._create_url(specific_path)


class HNAPI:
    @staticmethod
    def _get_data(url):
        try:
            response = requests.get(url)
            response_object = response.json()
            logging.debug(response_object)
            if response_object is None:
                raise ValueError("Recieved empty response")
            return response_object
        except Exception as e:
            logging.error(e)
            return None

    @staticmethod
    def get_top_stories():
        try:
            top_stories = HNAPI._get_data(HNUrlCreator().get_top_stories_url())
            if top_stories is None:
                raise ValueError("No relevant data")
            return top_stories
        except Exception as e:
            logging.error(e)
            raise ValueError("No relevant data")

    @staticmethod
    def get_item(item_id):
        return HNAPI._get_data(HNUrlCreator().get_item_url(item_id))


class Story:
    def __init__(self, story, rank):
        self.rank = rank
        self.title = story.get("title")
        self.url = story.get("url")
        self.type = story.get("type")
        self.score = story.get("score")

    def __str__(self):
        return f" Rank: {self.rank}, Title: {self.title}, Type: {self.type}, URL: {self.url}"


def create_story_objects(stories):
    try:
        return [Story(story_data, i+1) for i, story_data in enumerate(stories)]
    except Exception as e:
        logging.error(e)
        return stories


def get_stories_data(stories):
    try:
        return [HNAPI.get_item(str(story)) for story in stories]
    except Exception as e:
        logging.error(e)
        return stories


def filter_stories(stories):
    return list(filter(lambda story: story.type=="story",stories))


def print_stories(count):
    try:        
        top_stories = HNAPI.get_top_stories()
        logging.info(f"Top stories IDs: {top_stories}")
        logging.info("Getting full data for each story")
        stories = get_stories_data(top_stories[:count])
        logging.info("Got all the data")
        stories = create_story_objects(stories)
        for story in stories:
            logging.info(story)
    except Exception as e:
        logging.error(e)
        logging.error("Exiting")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Hacker news command line utility')
    parser.add_argument('--count', help='Count of stories to print', type=int,  default=40)
    args = parser.parse_args()
    logging.basicConfig(level=logging.INFO)
    logging.info("Process started")
    print_stories(args.count)

# Installation

1. You should have python installed on your machine.
2. Install requests library:
    `pip install requests`
3. Clone this repository:
    `git clone https://gitlab.com/agamliel/shellhn.git`
4. Get into the new repository: 
    `cd shellhn`
5. Run shellHN:
    `python src/hacker_new_api.py`
